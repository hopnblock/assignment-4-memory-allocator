#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define MAP_ANONYMOUS 0x20

#define DEFAULT_HEAP_SIZE 4096
#define EXPANDED_HEAP_SIZE (4096 * 3)
#define HEAP_START ((void*)0x04040000)

#define get_header(mem) ((struct block_header*) (((uint8_t*) (mem)) - offsetof(struct block_header, contents)))

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length,
                PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | additional_flags,
                -1, 0);
}

static void perform_test_one() {
    void *heap = heap_init(DEFAULT_HEAP_SIZE);
    assert(heap);
    void* allocated = _malloc(2048);
    assert(allocated);
    _free(allocated);
    heap_term();
}

static void perform_test_two() {
    void* heap = heap_init(0);
    assert(heap);
    void* first_block = _malloc(10);
    void* second_block = _malloc(20);
    void* third_block = _malloc(30);
    assert(first_block);
    assert(second_block);
    assert(third_block);
    _free(second_block);
    assert(!get_header(first_block)->is_free);
    assert(get_header(second_block)->is_free);
    assert(!get_header(third_block)->is_free);
    _free(first_block);
    _free(third_block);
    heap_term();
}

static void perform_test_three() {
    void *heap = heap_init(0);
    assert(heap);
    void *first_block = _malloc(10);
    void *second_block = _malloc(20);
    void *third_block = _malloc(30);
    assert(first_block);
    assert(second_block);
    assert(third_block);
    _free(first_block);
    _free(second_block);
    assert(get_header(first_block)->is_free);
    assert(get_header(second_block)->is_free);
    assert(!get_header(third_block)->is_free);
    _free(third_block);
    heap_term();
}

static void perform_test_four() {
    struct region *heap = heap_init(0);
    assert(heap);
    size_t initial_region_size = heap->size;
    _malloc(EXPANDED_HEAP_SIZE);
    size_t expanded_region_size = heap->size;
    assert(initial_region_size < expanded_region_size);
    heap_term();
}

void perform_test_five() {
    void *pre_allocated = map_pages(HEAP_START, 10, MAP_FIXED);
    assert(pre_allocated);
    void *allocated_filled_ptr = _malloc(10);
    assert(allocated_filled_ptr);
    assert(pre_allocated != allocated_filled_ptr);
    _free(allocated_filled_ptr);
    heap_term();
}

typedef void (*perform_test)();

const perform_test tests[] = {
        perform_test_one,
        perform_test_two,
        perform_test_three,
        perform_test_four,
        perform_test_five
};

static void print_test_success(size_t i) {
    printf("Test %zu: Success!\n", i);
}

const char* test_names[] = {
        "Test 1: Just Regular Allocation",
        "Test 2: Free A Block",
        "Test 3: Free Multiple Blocks",
        "Test 4: Expand Region",
        "Test 5: Expand A Filled Region"
};

static void print_test_name(size_t i) {
    printf("%s\n", test_names[i]);
}

void perform_tests() {
    size_t test_count = sizeof tests / sizeof tests[0];
    printf("Performing tests...\n");
    printf("--------------------------------------------------------------\n");
    for (size_t i = 0; i < test_count; i++) {
        print_test_name(i);
        tests[i]();
        print_test_success(i + 1);
        if (i != test_count - 1) printf("\n");
    }
    printf("--------------------------------------------------------------\n");
    printf("Tests complete.");
}
